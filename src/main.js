import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

import VueParticles from 'vue-particles'

Vue.use(VueParticles);

import VueI18n from 'vue-i18n'


/*Vue.use(VueI18n);
const i18n = new VueI18n({
    locale: 'fa', // set locale
    messages, // set locale messages
});*/
/*messages = {
    en: {
        message: {
            hello: 'hello world'
        }
    },
    fa: {
        message: {
            hello: 'こんにちは、世界'
        }
    }
},*/

// Create a Vue instance with `i18n` option


new Vue({
    router,
    store,
    // i18n,
    vuetify,
    render: function (h) {
        return h(App)
    }
}).$mount('#app');
