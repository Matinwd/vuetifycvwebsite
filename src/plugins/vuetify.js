import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

import fa from 'vuetify/src/locale/fa.ts'

export default new Vuetify({

});
